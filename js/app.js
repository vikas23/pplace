function createEle(tag, css, data) {
	var ele = document.createElement(tag),
		key;
	if (css) {
		ele.setAttribute('class', css);
	}
	for (key in data) {
		if (data.hasOwnProperty(key)) {
			ele[key] = data[key];
		}
	}
	return ele;
}

function makeAjaxCall(url, callback) {
	var http_request = new XMLHttpRequest();
	http_request.open("GET", url, true);
	http_request.onreadystatechange = function () {
		var done = 4,
			ok = 200;
		if (http_request.readyState == done && http_request.status == ok) {
			callback(JSON.parse(http_request.responseText));
		}
	};
	http_request.send();
}

function getData() {
	var str, queryInput = document.getElementById("searchPages"),
		searchFormRow = document.getElementsByClassName('form-search-row')[0],
		image = createEle('img', null, {
			'src': "img/ajax-loader.gif",
			'width': "30"
		}),
		access_token = "701600466607934|KZTKPBmBf8LR4DF0GwPWrzxQePI",
		url;

	if (!queryInput.value) {
		return;
	}

	str = encodeURIComponent(queryInput.value);
	searchFormRow.appendChild(image);

	url = "https://graph.facebook.com/search?type=page&q=" + str + "&access_token=" + access_token;

	function callback(data) {
		displayResults(data);
		image.parentNode.removeChild(image);
	}
	makeAjaxCall(url, callback);
}

function addEvent() {
	document.getElementById("searchPages").focus();
	document.getElementById("searchPages").onkeypress = function (e) {
		if (e.keyCode === 13) {
			getData();
		}
	};
}

function callback(data, img) {
	if (localStorage.getItem(data.id)) {
		localStorage.removeItem(data.id);
		img.src = "img/notselected.png"
	} else {
		img.src = "img/selected.png"
		localStorage.setItem(data.id, data.name);
	}
}

function checkLocalStore(id) {
	return localStorage[id];
}

function getFavPages() {
	var key,
		pages = {
			data: []
		};
	for (key in localStorage) {
		pages.data[pages.data.length] = {
			'id': key,
			'name': localStorage[key]
		}
	}
	displayResults(pages);
}

function displayResults(pages) {
	var resultDiv = document.getElementsByClassName('page-results')[0],
		i,
		data,
		len = pages.data.length;
	if (len) {
		resultDiv.innerHTML = "";
	} else {
		resultDiv.innerHTML = "No results found";
	}
	for (i = 0; i < len; i++) {
		data = pages.data[i];
		var page = createEle("div", 'span2 pageDiv'),
			findmore = createEle("a", 'gs-title', {
				'innerHTML': data.name,
				'href': "detail.html?id=" + data.id,
				'target': "_blank"
			}),
			likeImg = createEle("img", 'imgCss', {
				'src': checkLocalStore(data.id) ? "img/selected.png" : "img/notselected.png",
				'width': "15"
			});
		likeImg.onclick = (function (data, likeImg) {
			return function () {
				callback(data, likeImg);
			}
		})(data, likeImg);
		page.appendChild(findmore);
		page.appendChild(likeImg);
		resultDiv.appendChild(page);
	}
}


var getPageDeatil = function () {
	var query = window.location.search.substring(1);
	var vars = query.split("=");
	getDetailsAjax(vars[1]);
};

var getDetailsAjax = function (pageId) {
	var str = encodeURIComponent(pageId),
		access_token = "701600466607934|KZTKPBmBf8LR4DF0GwPWrzxQePI",
		url = "https://graph.facebook.com/" + str + "?access_token=" + access_token;
	makeAjaxCall(url, displayDetailsResult);
};

var displayDetailsResult = function (detail) {
	var resultDiv = document.getElementById('details');
	var li,
		key;
	for (key in detail) {
		if (detail.hasOwnProperty(key)) {
			li = createEle('li', "removeDecor", {
				'innerHTML': key + " : " + detail[key]
			});
			resultDiv.appendChild(li);
		}
	}
};
